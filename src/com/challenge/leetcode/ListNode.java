package com.challenge.leetcode;

/**
 * Definition for singly-linked list.
 *
 * @author Shakil Akhtar on 17/12/19
 */
public class ListNode {
     int val;
     ListNode next;
     ListNode(int x) { val = x; }
}
